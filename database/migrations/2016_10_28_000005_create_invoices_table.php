<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('customer')->unsigned();
            $table->integer('book')->unsigned();
            $table->date('date')->nullable();
            $table->integer('quantity')->default(0);
            $table->double('total')->default(0);
            $table->string('status', 255)->nullable();
            $table->timestamps();
            $table->foreign('customer')
                ->references('id')->on('customers');
            $table->foreign('book')
                ->references('id')->on('books');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
