<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category')->unsigned();
            $table->integer('genre')->unsigned();
            $table->string('title', 100);
            $table->string('author', 100)->nullable();
            $table->double('cost')->nullable();
            $table->double('price')->default(0);
            $table->integer('quantity')->default(0);
            $table->string('publisher', 100)->nullable();
            $table->string('language', 50)->nullable();
            $table->string('translator', 100)->nullable();
            $table->string('format', 50)->nullable();
            $table->date('date')->nullable();
            $table->integer('pages')->nullable();
            $table->float('weight')->nullable();
            $table->string('size', 50)->nullable();
            $table->string('image', 100);
            $table->text('detail')->nullable();
            $table->timestamps();
            $table->foreign('category')
                ->references('id')->on('categories');
            $table->foreign('genre')
                ->references('id')->on('genres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
