$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });

    $('#btn_reset').click(function(){
        $(this).closest('form').find("input[type=text], textarea").val("");
    });
    
	$('.panel-body').on('click', '.deleteId', function() {
        var id = $(this).attr('data_id');
        var table = $(this).attr('data_table');
        var url = $(location).attr('href').replace($(location).attr('pathname'), '/admin/tables/delete');
        $.post(url, {dataTable: table, dataID: id}, function(result) {
            if (result.success) {
                window.location.reload();
            }
        }, 'json');
    });

    $('.panel-body').on('click', '.updateId', function() {
        var id = $(this).attr('data_id');
        var table = $(this).attr('data_table');
        var url = $(location).attr('href').replace($(location).attr('pathname'), '/admin/tables/update');
        $.post(url, {dataTable: table, dataID: id}, function(result) {
            if (result.success) {
                $('body').html(result.html);
            }
        }, 'json');
    });

    $('#txt_quantity').keyup(function() {
        var book = $('#slt_book').val();
        var quantity = $('#txt_quantity').val();
        var url = $(location).attr('href').replace($(location).attr('pathname'), '/admin/forms/getPrice');
        if ($.isNumeric(quantity)) {
            $.post(url, {bookID: book, number: quantity}, function(result) {
                if (result.success) {
                    $('#txt_total').val(result.value);
                }
            }, 'json');
        }
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});