$(document).ready(function () {

    if ($("#alert").length) {
        $('#alert').delay(1000).fadeOut();
    }

    $('#sidebar .categories ul li a').click(function(e) {

        $('#sidebar .categories ul li a').removeClass('active');

        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active');
        }
        //e.preventDefault();
    });

    $('#signin').click(function() {
    	$('#signinPopup').show();
	});

	$('#signup').click(function() {
    	$('#signupPopup').show();
	});

    $('#signupPopup .closePopup').click(function() {
        $(".popup").hide();
    });

    $('#signinPopup .closePopup').click(function() {
        $(".popup").hide();
    });

    $(document).click(function(event) {
	    if ( event.target.className == 'popup') {
	    	$(".popup").hide();
	    }
	});

    $("#frmSignin").validate({
        rules: {
            signinUser: "required",
            signinPass: {
                required: true,
                minlength: 5
            },
        },

        messages: {
            signinUser: "Please enter your username",
            signinPass: {
                required: "Please enter your password",
                minlength: "Your password must be at least 5 characters long"
            },
        },

        submitHandler: function(form) {
          form.submit();
        }
    });

	$("#frmSignup").validate({
        rules: {
        	signupUser: {
                required: true,
                remote: {
                    type: "post",
                    url: "checkuser",
                    data: {
                        signupUser: function() {
                            return $('#signupUser').val()
                        }
                    },
                }
            },
        	signupPass: {
        		required: true,
        		minlength: 5
      		},
            signupRepass: {
                required: true,
                minlength: 5,
                equalTo : "#signupPass"
            },
            signupName: "required",
            signupEmail: {
                required: true,
                email: true
            }
        },

        messages: {
    	    signupUser: {
                required: "Please enter your username",
                remote: "Username invalid"
            },
    	    signupPass: {
    		    required: "Please enter your password",
    		    minlength: "Your password must be at least 5 characters long"
        	},
            signupRepass: {
                required: "Please re-enter your password",
                equalTo: "Passwords doesn't match"
            },
            signupName: "Please enter your name",
        	signupEmail: "Please enter a valid email address"
        },

        submitHandler: function(form) {
          form.submit();
        }
    });

    $('.buy-btn').click(function() {
        var id = $(this).attr('vcode');
        $.post("addcart", {bookID: id}, function(result){
            $("#numcart").html(result.numcart);
            $("#cart-details .sum").html(result.total);
        }, 'json');
    });

    $('#cart-details .info').click(function() {
        var id = $(this).attr('vcode');
        $.post("viewbook", {bookID: id}, function(result){
            $("body").html(result.html);
        }, 'json');
    });

    $('#cart-details .sum').click(function() {
        $('.order').slideToggle();
    });
});