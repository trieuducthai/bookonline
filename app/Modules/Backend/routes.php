<?php

$namespace = 'App\Modules\Backend\Controllers';

Route::group(['module'=>'Backend', 'namespace' => $namespace, 'prefix' => 'admin', 'middleware' => ['web']], function()
{
    
    Route::get('/', function()
    {
    	return view('Backend::pages.login');
    });

    Route::get('/login', 'BackendController@checkLogin');

    Route::group(['middleware' => 'admin'], function()
    {

        Route::get('/main', function()
        {
            return view('Backend::pages.main');
        });

        Route::get('/logout', 'BackendController@logout');

      	Route::get('forms', function()
        {
        	return view('Backend::pages.forms');
        });

        Route::group(['prefix' => 'tables'], function()
        {

            Route::get('books', 'PagesController@getBooks');

            Route::get('customers', 'PagesController@getCustomers');

            Route::get('invoices', 'PagesController@getInvoices');

            Route::post('delete', 'BackendController@deleteRow');

            Route::post('update', 'BackendController@updateRow');

        });

        Route::group(['prefix' => 'forms'], function()
        {

            Route::get('books', 'PagesController@getFormBooks');

            Route::get('customers', 'PagesController@getFormCustomers');

            Route::get('invoices', 'PagesController@getFormInvoices');

            Route::post('insertBook', 'BackendController@insertBook');

            Route::post('insertCustomer', 'BackendController@insertCustomer');

            Route::post('insertInvoice', 'BackendController@insertInvoice');

            Route::post('updateBook', 'BackendController@updateBook');

            Route::post('updateCustomer', 'BackendController@updateCustomer');

            Route::post('getPrice', 'BackendController@getPrice');

        });

    });

});
