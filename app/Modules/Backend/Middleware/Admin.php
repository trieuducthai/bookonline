<?php

namespace App\Modules\Backend\Middleware;

use Closure;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'users')
    {
        if(!Auth::guard($guard)->check()) {
            return redirect('admin');
        }
        return $next($request);
    }
}
