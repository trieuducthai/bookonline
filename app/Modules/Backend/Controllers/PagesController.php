<?php

namespace App\Modules\Backend\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Book;

use App\Models\Customer;

use App\Models\Invoice;

use App\Models\Category;

use App\Models\Genre;

class PagesController extends Controller
{
    var $books;
 	var $customers;
 	var $invoices;
    var $categories;
    var $genres;

    public function __construct() {
        $this->books = Book::all();
        $this->customers = Customer::all();
        $this->invoices = Invoice::all();
        $this->categories = Category::all();
        $this->genres = Genre::all();
    }

    public function getBooks() {
    	return view('Backend::pages.tables.books')->with('data', $this->books);
    }

    public function getCustomers() {
    	return view('Backend::pages.tables.customers')->with('data', $this->customers);
    }

    public function getInvoices() {
    	return view('Backend::pages.tables.invoices')->with('data', $this->invoices);
    }

    public function getFormBooks() {
	   return view('Backend::pages.forms.frmbooks', 
		['category' => $this->categories, 'genre' => $this->genres]);
    }

    public function getFormCustomers() {
    	return view('Backend::pages.forms.frmcustomers');
    }

    public function getFormInvoices() {
    	return view('Backend::pages.forms.frminvoices', 
    		['customer' => $this->customers, 'book' => $this->books]);
    }
}
