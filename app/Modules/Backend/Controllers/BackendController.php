<?php

namespace App\Modules\Backend\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Http\Response;

use App\Models\Book;

use App\Models\Customer;

use App\Models\Invoice;

use App\Models\Category;

use App\Models\Genre;

use Illuminate\Validation\Rule;

use Validator;

use URL;

use \DateTime;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;

class BackendController extends Controller
{
    var $categories;
    var $genres;

    public function __construct() {
    	define('BOOK_TABLE','books');
		define('CUSTOMER_TABLE','customers');
		define('INVOICE_TABLE','invoices');

        $this->categories = Category::all();
        $this->genres = Genre::all();
        $this->customers = Customer::all();
        $this->books = Book::all();
    }

    public function checkLogin(Request $request)
    {
        try {
            $email = $request['email'];
            $pass = $request['password'];
            if (Auth::guard('users')->attempt(['email'=>$email, 'password'=>$pass])) {
                $request->session()->put('admin', Auth::guard('users')->user()->name);
                return redirect('/admin/main');
            }
            else {
                return redirect('/admin');
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function logout()
    {
        Auth::guard('users')->logout();
        Session::flush();
        return redirect('/admin');
    }

    public function deleteRow(Request $request) {
    	$table = $request['dataTable'];
    	$id = $request['dataID'];
    	switch ($table) {
    		case BOOK_TABLE:
    			$book = Book::find($id);    
				$book->delete();
				$result = true;
    			break;
    		
			case CUSTOMER_TABLE:
    			$customer = Customer::find($id);    
				$customer->delete();
				$result = true;
    			break;

			case INVOICE_TABLE:
    			$invoice = Invoice::find($id);    
				$invoice->delete();
				$result = true;
    			break;

    		default:
    			$result = false;
    			break;
    	}
    	if ($result) {
    		return response()->json(array('success'=>true));
    	} else {
    		return response()->json(array('success'=>false));
    	}
    }

    public function updateRow(Request $request) {
    	$table = $request['dataTable'];
        $id = $request['dataID'];
        switch ($table) {
            case BOOK_TABLE:
                $book = Book::find($id);
                $returnHTML = view('Backend::pages.forms.frmbooks')->with(array('update' => true, 'tblData' => $book, 'category' => $this->categories, 'genre' => $this->genres))->render();
                break;
            
            case CUSTOMER_TABLE:
                $customer = Customer::find($id);
                $returnHTML = view('Backend::pages.forms.frmcustomers')->with(array('update' => true, 'tblData' => $customer))->render();
                break;

            case INVOICE_TABLE:
                $invoice = Invoice::find($id);
                $returnHTML = view('Backend::pages.forms.frminvoices')->with(array('update' => true, 'tblData' => $invoice, 'customer' => $this->customers, 'book' => $this->books))->render();
                break;

            default:
                $returnHTML = null;
                break;
        }
        if ($returnHTML !== null) {
            return response()->json(array('success' => true, 'html' => $returnHTML));
        } else {
            return response()->json(array('success' => false));
        }
    }

    public function insertBook(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_title' => 'required',
            'txt_cost' => 'required|numeric',
            'txt_price' => 'required|numeric',
            'txt_quantity' => 'required|numeric',
            'txt_date' => 'required|date',
            'txt_pages' => 'numeric',
            'txt_weight' => 'required|numeric',
            'txt_image' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/admin/forms/books')
                ->withInput()
                ->withErrors($validator);
        }
        else {
            try {
                $book = new Book;
                $book->category = $request->slt_category;
                $book->genre = $request->slt_genre;
                $book->title = $request->txt_title;
                $book->author = $request->txt_author;
                $book->cost = $request->txt_cost;
                $book->price = $request->txt_price;
                $book->quantity = $request->txt_quantity;
                $book->publisher = $request->txt_publisher;
                $book->language = $request->slt_language;
                $book->translator = $request->txt_translator;
                $book->format = $request->slt_format;
                $book->date = $request->txt_date;
                $book->pages = $request->txt_pages;
                $book->weight = $request->txt_weight;
                $book->size = $request->txt_size;
                $book->image = $request->txt_image;
                $book->detail = $request->txt_detail;
                $book->save();
                if ($book->save()) {
                    return redirect('/admin/forms/books')
                        ->with('success', true);
                } else {
                    return redirect('/admin/forms/books')
                        ->with('success', false);
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function insertCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_username' => 'required|min:3|max:20|unique:customers,username',
            'txt_password' => 'required|min:6',
            'txt_cfpassword' => 'required|same:txt_password',
            'txt_name' => 'required',
            'txt_email' => 'required|email|unique:customers,email',
            'txt_phone' => 'required|regex:/\+?[\d.]{1,3}\-?\(?[\d.]{2,4}\)?\-?[\d.]{2,4}\-?[\d]{2,6}/',
            'txt_address' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/forms/customers')
                ->withInput()
                ->withErrors($validator);
        }
        else {
            try {
                $customer = new Customer;
                $customer->username = $request->txt_username;
                $customer->password = bcrypt($request->txt_password);
                $customer->name = $request->txt_name;
                $customer->email = $request->txt_email;
                $customer->phone = $request->txt_phone;
                $customer->address = $request->txt_address;
     
                $customer->save();
                if ($customer->save()) {
                    return redirect('/admin/forms/customers')
                    ->with('success', true);
                } else {
                    return redirect('/admin/forms/customers')
                    ->with('success', false);
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function insertInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_date' => 'required|date',
            'txt_quantity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/forms/invoices')
                ->withInput()
                ->withErrors($validator);
        }
        else {
            try {

                $invoice = Invoice::where('customer', $request->slt_customer)->where('book', $request->slt_book)->first();
                $book = Book::find($request->slt_book);
                if ($invoice != "") {
                    $invoice->date = $request->txt_date;
                    $invoice->quantity += $request->txt_quantity;
                    $invoice->total = $book->price * $invoice->quantity;
                    $invoice->status = $request->txt_status;
                    $invoice->save();
                } else {
                    $invoice = new Invoice;
                    $invoice->customer = $request->slt_customer;
                    $invoice->book = $request->slt_book;
                    $invoice->date = (new DateTime($request->txt_date))->format('Y-m-d');
                    $invoice->quantity = $request->txt_quantity;
                    $invoice->total = $request->txt_total;
                    $invoice->status = $request->txt_status;
                    $invoice->save();
                }

                if ($invoice->save()) {
                    return redirect('/admin/tables/invoices')
                    ->with('success', true);
                } else {
                    return redirect('/admin/forms/invoices')
                    ->with('success', false);
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function updateBook(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_title' => 'required',
            'txt_cost' => 'required|numeric',
            'txt_price' => 'required|numeric',
            'txt_quantity' => 'required|numeric',
            'txt_date' => 'required|date',
            'txt_pages' => 'required|numeric',
            'txt_weight' => 'required|numeric',
            'txt_image' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/admin/forms/books')
                ->with('update', true)
                ->withInput()
                ->withErrors($validator);
        }
        else {
            try {
                $book = Book::find($request->txt_id);
                $book->category = $request->slt_category;
                $book->genre = $request->slt_genre;
                $book->title = $request->txt_title;
                $book->author = $request->txt_author;
                if ($request->txt_cost === '') {
                    $book->cost = null;   
                } else {
                    $book->cost = $request->txt_cost;
                }
                $book->price = $request->txt_price;
                $book->quantity = $request->txt_quantity;
                $book->publisher = $request->txt_publisher;
                $book->language = $request->slt_language;
                $book->translator = $request->txt_translator;
                $book->format = $request->slt_format;
                $book->date = $request->txt_date;
                $book->pages = $request->txt_pages;
                $book->weight = $request->txt_weight;
                $book->size = $request->txt_size;
                $book->image = $request->txt_image;
                $book->detail = $request->txt_detail;
                $book->save();
                if ($book->save()) {
                    return redirect('/admin/tables/books')
                        ->with('success', true);
                } else {
                    return redirect('/admin/forms/books')
                        ->with('success', false);
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function updateCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_username' => 'required|min:3|max:20',
            'txt_name' => 'required',
            'txt_email' => 'required|email',
            'txt_phone' => 'required|regex:/\+?[\d.]{1,3}\-?\(?[\d.]{2,4}\)?\-?[\d.]{2,4}\-?[\d]{2,6}/',
            'txt_address' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/forms/customers')
                ->with('update', true)
                ->withInput()
                ->withErrors($validator);
        }
        else {
            try {
                $customer = Customer::find($request->txt_id);
                $customer->username = $request->txt_username;
                $customer->name = $request->txt_name;
                $customer->email = $request->txt_email;
                $customer->phone = $request->txt_phone;
                $customer->address = $request->txt_address;
                $customer->save();
                if ($customer->save()) {
                    return redirect('/admin/tables/customers')
                    ->with('success', true);
                } else {
                    return redirect('/admin/forms/customers')
                    ->with('success', false);
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function getPrice(Request $request)
    {
        try {
            $code = $request->bookID;
            $num = $request->number;
            $book = Book::find($code);
            $total = $num * $book->price;
            if ($total !== null) {
                return response()->json(array('success' => true, 'value' => $total));
            } else {
                return response()->json(array('success' => false));
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

}
