@extends('Backend::templates.master')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ isset($tblData) ? 'Update Customer' : 'Add New Customer' }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="frmcustomer" name="frmcustomer" action="{{ (isset($update) || session('update')) ? url('admin/forms/updateCustomer') : url('admin/forms/insertCustomer') }}" method="post" role="form">
                            <div class="col-lg-6">
                                @if (isset($tblData))
                                <input type="hidden" name="txt_id" id="txt_id" value="{{ $tblData->id }}">
                                @endif
                                <div class="form-group">
                                    <label>Username</label>
                                    <input name="txt_username" id="txt_username" type="text" class="form-control" value="{{ isset($tblData) ? $tblData->username : old('txt_username') }}">
                                    @if ($errors->has('txt_username'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_username') }}
                                    </div>
                                    @endif
                                </div>
                                @if (!isset($tblData))
                                <div class="form-group">
                                    <label>Password</label>
                                    <input name="txt_password" id="txt_password" type="password" class="form-control" value="{{ old('txt_password') }}">
                                    @if ($errors->has('txt_password'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_password') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input name="txt_cfpassword" id="txt_cfpassword" type="password" class="form-control" value="{{ old('txt_cfpassword') }}">
                                    @if ($errors->has('txt_cfpassword'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_cfpassword') }}
                                    </div>
                                    @endif
                                </div>
                                @endif
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name="txt_name" id="txt_name" type="text" class="form-control" value="{{ isset($tblData) ? $tblData->name : old('txt_name') }}">
                                    @if ($errors->has('txt_name'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_name') }}
                                    </div>
                                    @endif
                                </div>        
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input name="txt_email" id="txt_email" type="text" class="form-control" value="{{ isset($tblData) ? $tblData->email : old('txt_email') }}">
                                    @if ($errors->has('txt_email'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_email') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input name="txt_phone" id="txt_phone" type="text" class="form-control" value="{{ isset($tblData) ? $tblData->phone : old('txt_phone') }}">
                                    @if ($errors->has('txt_phone'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_phone') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea name="txt_address" id="txt_address" class="form-control" cols="3">{{ isset($tblData) ? $tblData->address : old('txt_address') }}</textarea>
                                    @if ($errors->has('txt_address'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_address') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button name="btn_submit" id="btn_submit" type="submit" class="btn btn-default">OK</button>
                                    <button name="btn_reset" id="btn_reset" type="reset" class="btn btn-default">Reset</button>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection