@extends('Backend::templates.master')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ isset($tblData) ? 'Update Book' : 'Add New Book' }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Element
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form name="frm_book" id="frm_book" action="{{ (isset($update) || session('update')) ? url('admin/forms/updateBook') : url('admin/forms/insertBook') }}" method="post" role="form">
                            <div class="col-lg-6">
                            @if (isset($tblData))
                                <input type="hidden" name="txt_id" id="txt_id" value="{{ $tblData->id }}">
                            @endif
                                <div class="form-group"> 
                                    <label>Book name</label>
                                    <input type="text" name="txt_title" id="txt_title" class="form-control" value="{{ isset($tblData) ? $tblData->title : old('txt_title') }}">
                                    @if ($errors->has('txt_title'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_title') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="slt_category" id="slt_category" class="form-control">
                                    @foreach ($category as $value)
                                        <option value="{{$value->id}}" {{ (isset($tblData) && $tblData->id === $value->id) ? 'selected' : '' }}>{{$value->category}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                   <label>Genre</label>
                                    <select name="slt_genre" id="slt_genre" class="form-control">
                                    @foreach ($genre as $value)
                                        <option value="{{$value->id}}" {{ (isset($tblData) && $tblData->id === $value->id) ? 'selected' : '' }}>{{$value->genre}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Author</label>
                                    <input type="text" name="txt_author" id="txt_author" class="form-control" value="{{ isset($tblData) ? $tblData->author : old('txt_author') }}">
                                </div>
                                <div class="form-group">
                                    <label>Cost</label>
                                    <input type="text" name="txt_cost" id="txt_cost" class="form-control" value="{{ isset($tblData) ? $tblData->cost : old('txt_cost') }}">
                                    @if ($errors->has('txt_cost'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_cost') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" name="txt_price" id="txt_price" class="form-control" value="{{ isset($tblData) ? $tblData->price : old('txt_price') }}">
                                    @if ($errors->has('txt_price'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_price') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input type="text" name="txt_quantity" id="txt_quantity" class="form-control" value="{{ isset($tblData) ? $tblData->quantity : old('txt_quantity') }}">
                                    @if ($errors->has('txt_quantity'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_quantity') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Publisher</label>
                                    <input type="text" name="txt_publisher" id="txt_publisher" class="form-control" value="{{ isset($tblData) ? $tblData->publisher : old('txt_publisher') }}">
                                </div>  
                                <div class="form-group">
                                    <label>Translator</label>
                                    <input type="text" name="txt_translator" id="txt_translator" class="form-control" value="{{ isset($tblData) ? $tblData->translator : old('txt_translator') }}">
                                </div>     
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                   <label>Language</label>
                                    <select name="slt_language" id="slt_language" class="form-control">
                                        <option value="VI" {{ (isset($tblData) && $tblData->language === 'VI') ? 'selected' : '' }}>Tiếng Việt</option>
                                        <option value="EN" {{ (isset($tblData) && $tblData->language === 'EN') ? 'selected' : '' }}>Tiếng Anh</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                   <label>Format</label>
                                    <select name="slt_format" id="slt_format" class="form-control">
                                        <option value="BM" {{ (isset($tblData) && $tblData->format === 'BM') ? 'selected' : '' }}>Bìa mềm</option>
                                        <option value="BC" {{ (isset($tblData) && $tblData->format === 'BC') ? 'selected' : '' }}>Bìa cứng</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Release Date</label>
                                    <input type="date" name="txt_date" id="txt_date" class="form-control" value="{{ isset($tblData) ? $tblData->date : old('txt_date') }}">
                                    @if ($errors->has('txt_date'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_date') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Total Pages</label>
                                    <input type="text" name="txt_pages" id="txt_pages" class="form-control" value="{{ isset($tblData) ? $tblData->pages : old('txt_pages') }}">
                                    @if ($errors->has('txt_pages'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_pages') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Weight</label>
                                    <input type="text" name="txt_weight" id="txt_weight" class="form-control" value="{{ isset($tblData) ? $tblData->weight : old('txt_weight') }}">
                                    @if ($errors->has('txt_weight'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_weight') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Size</label>
                                    <input type="text" name="txt_size" id="txt_size" class="form-control" value="{{ isset($tblData) ? $tblData->size : old('txt_size') }}">
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" accept="image/*" name="txt_image" id="txt_image" class="form-control" value="{{ isset($tblData) ? $tblData->image : old('txt_image') }}">
                                    @if ($errors->has('txt_image'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_image') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Detail</label>
                                    <textarea name="txt_detail" id="txt_detail" class="form-control" rows="4">{{ isset($tblData) ? $tblData->detail : old('txt_detail') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button name="btn_submit" id="btn_submit" type="submit" class="btn btn-default">OK</button>
                                    <button name="btn_submit" id="btn_reset" type="reset" class="btn btn-default">Reset</button>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection