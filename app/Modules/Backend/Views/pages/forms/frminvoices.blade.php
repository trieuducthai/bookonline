@extends('Backend::templates.master')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ isset($tblData) ? 'Update Invoice' : 'Add New Invoice' }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="frmcustomer" name="frmcustomer" action="{{ url('admin/forms/insertInvoice') }}" method="post" role="form">
                            <div class="col-lg-6">
                            @if (isset($tblData))
                                <input type="hidden" name="txt_id" id="txt_id" value="{{ $tblData->id }}">
                            @endif
                                <div class="form-group">
                                    <label>Customer</label>
                                    <select name="slt_customer" id="slt_customer" class="form-control">
                                    @foreach($customer as $value)
                                        <option value="{{$value->id}}" {{ (isset($tblData) && $tblData->customer === $value->id) ? 'selected' : '' }}>{{ $value->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Book</label>
                                    <select name="slt_book" id="slt_book" class="form-control">
                                    @foreach($book as $value)
                                        <option value="{{$value->id}}" {{ (isset($tblData) && $tblData->book === $value->id) ? 'selected' : '' }}>{{ $value->title }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input type="text" name="txt_quantity" id="txt_quantity" class="form-control" value="{{ isset($tblData) ? $tblData->quantity : old('txt_quantity') }}">
                                    @if ($errors->has('txt_quantity'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_quantity') }}
                                    </div>
                                    @endif
                                </div>            
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="txt_total" id="txt_total" class="form-control" readonly='readonly' value="{{ isset($tblData) ? $tblData->total : '0' }}">
                                </div>
                                <div class="form-group">
                                    <label>Date</label>
                                    <input type="date" name="txt_date" id="txt_date" class="form-control" value="{{ isset($tblData) ? $tblData->date : old('txt_date') }}">
                                    @if ($errors->has('txt_date'))
                                    <div class="input-error">
                                        {{ $errors->first('txt_date') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <textarea name="txt_status" id="txt_status" class="form-control" rows="3">{{ isset($tblData) ? $tblData->status : old('txt_status') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button name="btn_submit" id="btn_submit" type="submit" class="btn btn-default">OK</button>
                                    <button name="btn_reset" id="btn_reset" type="reset" class="btn btn-default">Reset</button>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection