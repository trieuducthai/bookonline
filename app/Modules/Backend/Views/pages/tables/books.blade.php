@extends('Backend::templates.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Book Table</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        DataTables Advanced Tables
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>Author</th>
                                    <th>Cost</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Publisher</th>
                                    <th>Translator</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $value)
                                <tr>
                                    <td>{{ $value->title }}</td>
                                    <td>{{ $value->author }}</td>
                                    <td>{{ $value->cost }}</td>
                                    <td>{{ $value->price }}</td>
                                    <td>{{ $value->quantity }}</td>
                                    <td>{{ $value->publisher }}</td>
                                    <td>{{ $value->translator }}</td>
                                    <td>
                                        <a href='#' data_id='{{ $value->id }}' data_table='books' class='deleteId'>Delete</a> |
                                        <a href='#' data_id='{{ $value->id }}' data_table='books' class='updateId'>Update</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

@endsection