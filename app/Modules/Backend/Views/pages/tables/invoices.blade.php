@extends('Backend::templates.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Invoice Table</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        DataTables Advanced Tables
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Books</th>
                                    <th>Date</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $value)
                                <tr>
                                    <td>{{ $value->customer }}</td>
                                    <td>{{ $value->book }}</td>
                                    <td>{{ $value->date }}</td>
                                    <td>{{ $value->quantity }}</td>
                                    <td>{{ $value->total }}</td>
                                    <td>{{ $value->status }}</td>
                                    <td>
                                        <a href='#' data_id='{{ $value->id }}' data_table='invoices' class='deleteId'>Delete</a> |
                                        <a href='#' data_id='{{ $value->id }}' data_table='invoices' class='updateId'>Update</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

@endsection