<?php

namespace App\Modules;
 
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Components\Assets\Assets;
use Request;
 
class ModuleServiceProvider extends ServiceProvider
{
    
    public function register()
    {
        //code
    }
    
    public function boot()
    {
        $modules = config('module.modules');
        if (Request::is('admin') || Request::is('admin/*')) {
            $mod = $modules['admin'];
        } else {
            $mod = $modules['site'];
        }

        if (file_exists(__DIR__ . '/' . $mod . '/routes.php')) {
            include __DIR__ . '/' . $mod . '/routes.php';
        }
 
        if (is_dir(__DIR__ . '/' . $mod . '/Views')) {
            $this->loadViewsFrom(__DIR__ . '/' . $mod . '/Views', $mod);
        }
    }

}

?>
