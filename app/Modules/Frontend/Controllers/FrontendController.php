<?php

namespace App\Modules\Frontend\Controllers;

use Carbon\Carbon;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Http\Response;

use App\Models\Book;

use App\Models\Customer;

use App\Models\Invoice;

use App\Models\Category;

use App\Models\Genre;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;

class FrontendController extends Controller
{
    var $categories;
    var $genres;
    var $guest;
    var $guestEmail;

    public function __construct() {
        $this->categories = Category::all();
        $this->genres = Genre::all();
        $this->guest = "Guest";
        $this->guestEmail = "@gmail.com";

    }

    public function insertCustomer(Request $request)
    {
        try {
        	$customer = new Customer;
            $customer->username = $request->signupUser;
            $customer->password = bcrypt($request->signupPass);
            $customer->name = $request->signupName;
            $customer->email = $request->signupEmail;
            $customer->phone = $request->signupPhone;
            $customer->address = $request->signupAddress;
            $customer->save();
            return redirect('/')->with('status', 'Registration Successfully!');
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function checkUser(Request $request)
    {
        try {
            $user = $request['signupUser'];
            $userdata = Customer::where('username', $user)->first();
            if ($userdata != null) {
                echo 'false';
            }
            else {
                echo 'true';
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function checkLogin(Request $request)
    {
        try {
    	$user = $request['signinUser'];
    	$pass = $request['signinPass'];
    	if (Auth::guard('persons')->attempt(['username'=>$user, 'password'=>$pass])) {
            $request->session()->put('userID', Auth::guard('persons')->user()->id);
    		$request->session()->put('userName', Auth::guard('persons')->user()->name);
            $request->session()->put('quantity', Invoice::where('customer', Auth::guard('persons')->user()->id)->sum('quantity'));
            $request->session()->put('total', Invoice::where('customer', Auth::guard('persons')->user()->id)->sum('total'));
            return redirect('/')->with('status', 'Login Successfully!');
        }
        else {
            return redirect('/')->with('status', 'Login fail!');
        }
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function logout()
    {
        Auth::guard('persons')->logout();
        Session::flush();
        return redirect('/');
    }

    public function addCart(Request $request)
    {
        try {
            
            if ($request->session()->has('userName')) {
                $userID = $request->session()->get('userID');
            } else {
                $randUser = str_random(10);
                $customer = new Customer;
                $customer->username = $randUser;
                $customer->password = bcrypt($randUser);
                $customer->name = $this->guest;
                $customer->email = $randUser.$this->guestEmail;
                $customer->save();
                $request->session()->put('userID', $customer->id);
                $request->session()->put('userName', $customer->name);
                $request->session()->put('quantity', Invoice::where('customer', $customer->id)->sum('quantity'));
                $request->session()->put('total', Invoice::where('customer', $customer->id)->sum('total'));
                $userID = $customer->id;
            }

            $bookID = $request['bookID'];
            $book = Book::find($bookID);
            $invoice = Invoice::where('customer', $userID)->where('book', $bookID)->first();

            if ($invoice != "") {
                $invoice->date = Carbon::now()->format('Y-m-d');
                $invoice->quantity += 1;
                $invoice->total = $book->price * $invoice->quantity;
                $invoice->status = "";
                $invoice->save();
            } else {
                $invoice = new Invoice;
                $invoice->customer = $userID;
                $invoice->book = $bookID;
                $invoice->date = Carbon::now()->format('Y-m-d');
                $invoice->quantity = 1;
                $invoice->total = $book->price * $invoice->quantity;
                $invoice->status = "";
                $invoice->save();
            }

            $request->session()->forget('quantity');
            $request->session()->forget('total');
            $request->session()->forget('order');
            $quantity = Invoice::where('customer', $userID)->sum('quantity');
            $total = Invoice::where('customer', $userID)->sum('total');
            $order = Invoice::where('customer', $userID)->first();
            $request->session()->put('quantity', $quantity);
            $request->session()->put('total', $total);
            $request->session()->put('order', $order);
            return response()->json(['numcart' => $quantity, 'total' => $total.'đ', 'order' => $order]);

        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function viewBook(Request $request)
    {
        try {
            $book = Book::find($request['bookID']);
            $returnHTML = view('Frontend::pages.book')->with(array('book' => $book, 'category' => $this->categories, 'genre' => $this->genres))->render();
            return response()->json(array('html' => $returnHTML));
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
}
