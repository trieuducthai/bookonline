<?php

namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Book;

use App\Models\Category;

use App\Models\Genre;

class PagesController extends Controller
{
    var $books;
    var $categories;
    var $types;

    public function __construct() {
        $this->books = Book::paginate(8);
        $this->categories = Category::all();
        $this->genres = Genre::all();
    }

	//Get view main
    public function main()
    {
        return view('Frontend::main', 
            ['book' => $this->books, 'category' => $this->categories, 'genre' => $this->genres]);
    }

    //Get view svhnn
    public function svhnn()
    {
        $this->books = Book::paginate(12);
        return view('Frontend::pages.svhnn', 
            ['book' => $this->books, 'category' => $this->categories, 'genre' => $this->genres]);
    }

    //Get view about
    public function about()
    {
    	return view('Frontend::pages.about');
    }

    //Get view contact
    public function contact()
    {
    	return view('Frontend::pages.contact');
    }

}
