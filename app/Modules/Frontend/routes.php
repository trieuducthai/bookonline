<?php

$namespace = 'App\Modules\Frontend\Controllers';

Route::group(['module'=>'Frontend', 'namespace' => $namespace, 'middleware' => ['web']], function() {

		Route::get('/', 'PagesController@main');

		Route::get('category/1', 'PagesController@svhnn');

		Route::get('getJson', 'FrontendController@getJson');

		Route::get('about', 'PagesController@about');

		Route::get('contact', 'PagesController@contact');

		Route::post('signup', ['as' => 'signup', 'uses' => 'FrontendController@insertCustomer']);

		Route::post('login', ['as' => 'login', 'uses' => 'FrontendController@checkLogin']);

		Route::post('checkuser', ['as' => 'check', 'uses' => 'FrontendController@checkUser']);

		Route::get('logout', 'FrontendController@logout');

		Route::post('addcart', ['as' => 'addcart', 'uses' => 'FrontendController@addCart']);

		Route::post('viewbook', ['as' => 'viewbook', 'uses' => 'FrontendController@viewBook']);
});
    
