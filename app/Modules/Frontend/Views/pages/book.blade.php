@extends('Frontend::templates.master')
 
@section('products')
<div class="products">
    <h3>Book Infomation</h3>
    <div class="book-info">
    	<h4>{{ $book->title }}</h4>
    	<p class="book-author">Tác giả: {{ $book->author }}</p>
		<p class="book-translator">Người dịch: {{ $book->translator }}</p>
		<p class="book-publisher">Nhà xuất bản: {{ $book->publisher }}</p>
		<p class="book-cost">Giá bìa: {{ $book->cost }}</p>
		<p class="book-price">Giá bán: {{ $book->price }}</p>
        <p class="book-quantity">Số lượng: {{ $book->quantity }}</p>
        <p class="book-publisher">Nhà xuất bản: {{ $book->publisher }}</p>
        <p class="book-language">Ngôn ngữ: {{ $book->language }}</p>
        <p class="book-redate">Ngày phát hành: {{ $book->date }}</p>
        <p class="book-pages">Tổng số trang: {{ $book->price }}</p>
        <p class="book-weight">Khối lượng: {{ $book->pages }}</p>
        <p class="book-size">Kích thước: {{ $book->size }}</p>
    </div>
    <div class="book-info">
        <img src="{{asset('images/books/'.$book->image)}}" alt="" />
        <a href = "#">BUY NOW</a>
    </div>
    <div class="book-info">
        <p class="book-detail">Nội dung: {{ $book->detail }}</p>
    </div>
</div>
@endsection