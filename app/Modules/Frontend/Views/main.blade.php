@extends('Frontend::templates.master')

@section('products')
<div class="products">
    <h3>Featured Products</h3>
    <ul>
    @foreach($book as $value)
        <li>
            <div class="product">
                <a href="#" vcode="{{$value->id}}" class="info">
                    <span class="holder">
                        <img src="{{asset('images/books/'.$value->image)}}" alt="" />
                        <span class="book-name">{{$value->title}}</span>
                        <span class="author">{{$value->author}}</span>
                        <span class="cost">
                        @if($value->cost != '')
                            {{$value->cost . ' đ'}}
                        @endif
                        </span>
                        <span class="price">
                        @if($value->price != '')
                            {{$value->price . ' đ'}}
                        @endif
                        </span>
                    </span>
                </a>
                <a href="#" vcode="{{$value->id}}" class="buy-btn">ADD TO CART <span class="add-cart">❯❯</span></a>
            </div>
        </li>
    @endforeach
    </ul>
</div>
@endsection

@section('best-sellers')
<div id="best-sellers">
    <h3>Best Sellers</h3>
    <ul>
    @foreach($book as $value)
        <li>
            <div class="product">
                <a href="#" vcode="{{$value->id}}" class="info">
                    <img src="{{asset('images/books/'.$value->image)}}" alt="" />
                    <span class="book-name">{{$value->title}}</span>
                    <span class="author">{{$value->author}}</span>
                    @if($value->cost != '')
                    <span class="price">
                    <span class="low">-</span>
                        {{100-round((($value->price)/($value->cost))*100)}}
                        <span class="high">%</span>
                    </span>
                    @endif
                </a>
            </div>
        </li>
    @endforeach
    </ul>
</div>
@endsection