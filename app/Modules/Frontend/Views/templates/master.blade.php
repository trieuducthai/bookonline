<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
	<title>Book Online</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="csrf-token" content="{{csrf_token()}}" />
	<link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
	<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="all" />
</head>
<body>
	<!-- Header -->
	<div id="header" class="shell">
		<div id="logo"><h1><a href="/">BookOnline</a></h1><span>buy your books</span></a></div>
		<!-- Navigation -->
		<div id="navigation">
			<ul>
				<li><a href="#" class="active">Home</a></li>
				<li><a href="#">Products</a></li>
				<li><a href="#">Promotions</a></li>
				<li><a href="#">Profile</a></li>
				<li><a href="#">About Us</a></li>
				<li><a href="#">Contacts</a></li>
			</ul>
		</div>
		<!-- End Navigation -->
		<div class="cl">&nbsp;</div>
		<!-- Login-details -->
		<div id="login-details">
        @if (session('userName'))
            <p>Welcome, <a href="#" id="user">{{ session('userName') }}</a></p>
            <div class="login">
                <a href="/logout" id="logout">Logout</a>
            </div>
        @else
            <p>Welcome, <a href="#" id="user">Guest</a></p>
            <div class="login">
                <a href="#" id="signin">Login</a> - 
                <a href="#" id="signup">Register</a>
            </div>
        @endif
		</div>
		<!-- End Login-details -->
		<!-- Alert -->
		@if (session('status'))
		    <div id="alert">
		        {{ session('status') }}
		    </div>
		@endif
		<!-- End Alert -->
		<!-- Cart-details -->
		<div id="cart-details">
		@if (session('userID'))
			<div>
				<a href="#" class="cart" ><img src="{{ asset('images/cart-icon.png') }}" alt="cart" /></a>
				Shopping Cart (<span id="numcart">{{ session('quantity') }}</span>)
			</div>
			<div class="total">Total: <a href="#" class="sum">{{ session('total') . 'đ' }}</a></div>
			<div class="order">
			{{session('order')}}
			@if (isset($order))
			@foreach($order as $value)
				<img src="{{ asset('images/books/'.$order->book) }}" alt="product">
				<span class="number">{{ $value->quantity }}</span>
				<span class="total-amount">{{ $value->total }}</span>
			@endforeach
			@endif
			</div>
		@else
			<div>
				<a href="#" class="cart" ><img src="{{ asset('images/cart-icon.png') }}" alt="cart" /></a>
				Shopping Cart (<span id="numcart">0</span>)
			</div>
			<div class="total">Total: <a href="#" class="sum">0 đ</a></div>
			<div class="order">
				{{'Empty Cart'}}
			</div>
		@endif
		</div>
		<!-- End Cart-details -->
		<!-- Signin-popup -->
		<div id="signinPopup" class="popup">
			<div class="signin-form">
			<label class="closePopup">X</label>
			<h3>SIGN IN</h3>
			<form action="{{route('login')}}" name="frmSignin" id="frmSignin" method="post">
				<div class="input">
					<label for="loginUser">Username:</label>
					<input type="text" name="signinUser" id="signinUser">
				</div>
				<div class="input">
					<label for="loginPass">Password:</label>
					<input type="password" name="signinPass" id="signinPass">
				</div>
				<div class="input">
					<button type="submit" name="btnLogin" id="btnLogin">Login</button>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</form>
			</div>
		</div>
		<!-- End Signin-popup -->
		<!-- Signup-popup -->
		<div id="signupPopup" class="popup">
			<div class="signup-form">
			<label class="closePopup">X</label>
			<h3>SIGN UP</h3>
			<form action="{{route('signup')}}" name="frmSignup" id="frmSignup" method="post">
				<div class="input">
					<label for="signupUser">Username:</label>
					<input type="text" name="signupUser" id="signupUser">
				</div>
				<div class="input">
					<label for="signupPass">Password:</label>
					<input type="password" name="signupPass" id="signupPass">
				</div>
				<div class="input">
					<label for="signupPepass">Confirm password:</label>
					<input type="password" name="signupRepass" id="signupRepass">
				</div>
				<div class="input">
					<label for="signupName">Name:</label>
					<input type="text" name="signupName" id="signupName">
				</div>
				<div class="input">
					<label for="signupEmail">Email:</label>
					<input type="text" name="signupEmail" id="signupEmail">
				</div>
				<div class="input">
					<label for="signupPhone">Phone number:</label>
					<input type="text" name="signupPhone" id="signupPhone">
				</div>
				<div class="input">
					<label for="signupAddress">Address:</label>
					<input type="text" name="signupAddress" id="signupAddress">
				</div>
				<div class="input">
					<button type="submit" name="btnSignup" id="btnSignup">Signup</button>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</form>
			</div>
		</div>
		<!-- End Signup-popup -->
	</div>
	<!-- End Header -->
	<!-- Slider -->
	<div id="slider">
    	<div class="shell">
        	<ul>
	        @for( $i = 0; $i < 4 ; $i++ )
	            <li>
	                <div class="image">
	                    <img src="{{asset('images/books.png')}}" alt="" />
	                </div>
	                <div class="details">
	                    <h2>Bestsellers</h2>
	                    <h3>Special Offers</h3>
	                    <p class="title">Pellentesque congue lorem quis massa blandit non pretium nisi pharetra</p>
	                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id odio in tortor scelerisque dictum. Phasellus varius sem sit amet metus volutpat vel vehicula nunc lacinia.</p>
	                    <a href="#" class="read-more-btn">Read More</a>
	                </div>
	            </li>
	        @endfor
        	</ul>
        <div class="nav">
        @for( $i = 0; $i < 4 ; $i++ )
            <a href="#">{{$i+1}}</a>
        @endfor
        </div>
    </div>
</div>
	<!-- End Slider -->
	<!-- Main -->
	<div id="main" class="shell">
		<!-- Sidebar -->
			<div id="sidebar">
			   <ul class="categories">
			        <li>
			            <h4>Categories</h4>
			            <ul>
			                @foreach ($category as $value)
			                    <li><a href="{{ url('category/'.$value->id) }}">{{ $value->category }}</a></li>
			                @endforeach
			            </ul>
			        </li>
			        <li>
			            <h4>Genres</h4>
			            <ul>
			                @foreach($genre as $value)
			                    <li><a href="{{ url('genre/'.$value->id) }}">{{ $value->genre }}</a></li>
			                @endforeach
			            </ul>
			        </li>
			    </ul>
			</div>
		<!-- End Sidebar -->
		<!-- Content -->
		<div id="content">
			<!-- Products -->
				@yield('products')
			<!-- End Products -->
			<div class="cl">&nbsp;</div>
			<!-- Best-sellers -->
		    	@yield('best-sellers')
			<!-- End Best-sellers -->
		</div>
		<!-- End Content -->
		<div class="cl">&nbsp;</div>
	</div>
	<!-- End Main -->
	<!-- Footer -->
	<div id="footer" class="shell">
		<div class="top">
			<div class="cnt">
				<div class="col about">
					<h4>About BookOnline</h4>
					<p>Nulla porttitor pretium mattis. Mauris lorem massa, ultricies non mattis bibendum, semper ut erat. Morbi vulputate placerat ligula. Fusce <br />convallis, nisl a pellentesque viverra, ipsum leo sodales sapien, vitae egestas dolor nisl eu tortor. Etiam ut elit vitae nisl tempor tincidunt. Nunc sed elementum est. Phasellus sodales viverra mauris nec dictum. Fusce a leo libero. Cras accumsan enim nec massa semper eu hendrerit nisl faucibus. Sed lectus ligula, consequat eget bibendum eu, consequat nec nisl. In sed consequat elit. Praesent nec iaculis sapien. <br />Curabitur gravida pretium tincidunt.  </p>
				</div>
				<div class="col store">
					<h4>Store</h4>
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Special Offers</a></li>
						<li><a href="#">Log In</a></li>
						<li><a href="#">Account</a></li>
						<li><a href="#">Basket</a></li>
						<li><a href="#">Checkout</a></li>
					</ul>
				</div>
				<div class="col" id="newsletter">
					<h4>Newsletter</h4>
					<p>Lorem ipsum dolor sit amet  consectetur. </p>
					<form action="" method="post">
						<input type="text" class="field" value="Your Name" title="Your Name" />
						<input type="text" class="field" value="Email" title="Email" />
						<div class="form-buttons"><input type="submit" value="Submit" class="submit-btn" /></div>
					</form>
				</div>
				<div class="cl">&nbsp;</div>
				<div class="copy">
					<p>&copy; <a href="#">BookOnline.com</a>. Design by <a href="#">TDT</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- End Footer -->
	<script type="text/javascript" src="{{asset('js/jquery-1.6.2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.jcarousel.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/functions.js')}}"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
	<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
	<script type="text/javascript" src="{{asset('js/frontend.js')}}"></script>
	<script type="text/javascript">
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  	</script>
	<!--[if IE 6]>
		<script type="text/javascript" src="{{asset('js/png-fix.js')}}"></script>
	<![endif]-->
</body>
</html>